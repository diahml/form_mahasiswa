import Link from 'next/link'
import React from 'react'


const getMahasiswa = async () => {
  const res = await fetch(process.env.BASE_URL + '/api/mahasiswa', { next: { revalidate: 0 } })
  const json = await res.json()
  return json
}
 

 
const Page = async () =>{
  const mahasiswa = await getMahasiswa()

  return (
    <div className="ml-10 mr-10 mt-10 mb-10 rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
      <div className="py-6 px-4 md:px-6 xl:px-7.5">
      <h4 className="text-xl font-semibold text-black dark:text-white">
          Data Diri Mahasiswa
        </h4>
      </div>

      <Link href={"/"} className='ml-4 px-3 py-2 bg-zinc-900 hover:bg-zinc-800 rounded-md text-white'>Tambah Data Mahasiswa</Link>
     
      <div className="flex flex-col">
      <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
          <div className="overflow-hidden">
      <table className="min-w-full text-left text-sm font-light">
        <thead className="border-b font-medium dark:border-neutral-500">
          <tr>
            <th scope="col" className="px-6 py-4">No</th>
             <th scope="col" className="px-6 py-4">Nama</th>
             <th scope="col" className="px-6 py-4">NIM</th>
             <th scope="col" className="px-6 py-4">Email</th>
          </tr>
        </thead>
        <tbody>
        {mahasiswa?.mahasiswa?.map((mahasiswa: any, index: number) => (
          <tr key={index} className="border-b dark:border-neutral-500">
          <td className="whitespace-nowrap px-6 py-4 font-medium">
            {index+1}
          </td>
          <td className="whitespace-nowrap px-6 py-4">
           {mahasiswa.nama}
          </td>
          <td className="whitespace-nowrap px-6 py-4">
            {mahasiswa.nim}
          </td>
          <td className="whitespace-nowrap px-6 py-4">
            {mahasiswa.email}
          </td>
        </tr>
        ))}
              
        
        </tbody>
      </table>
      </div>
        </div>
      </div>
    </div>
      </div>
     
  );
}
export default Page