"use client"
import { useRouter } from 'next/navigation'
import React, { useState } from 'react'
import { CldUploadButton, CldUploadWidgetProps, CldUploadWidgetResults } from 'next-cloudinary'
import { setupFsCheck } from 'next/dist/server/lib/router-utils/filesystem'

const Page = () => {
    const [nama, setNama] = useState('')
    const [email, setEmail] = useState('')
    const [nim, setNim] = useState('')
    const [jenis_kelamin, setJenisKelamin] = useState('')
    const [alamat, setAlamat] = useState('')
    const [telp, setTelp] = useState('')
    const [image, setImage] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const router = useRouter()

    const handleSubmit = async (e:any)=>{
      e.preventDefault()

      setIsLoading(true)

      await fetch("/api/mahasiswa",{
        method:"POST",
        headers:{
          "Content-Type":"application/json"
        },
        body: JSON.stringify({
          nama, nim, email, jenis_kelamin, alamat, telp, image
        })
      }).then((res)=>{
        console.log(res);

      }).catch((e)=>{
        console.log(e);
      })

      setIsLoading(false)
      window.location.replace('/tabel')
    }

    

    return (
      
        <form className='ml-10 mr-10 pt-20 flex flex-col gap-2 mb-10' onSubmit={handleSubmit}>
            <div>
              <h3 className="block justify-center font-medium text-black dark:text-white">
                FORM PENDAFTARAN MAHASISWA
              </h3>
            </div>
            <div>
            <label className="pl-1 block text-black dark:text-white">
                  Nama Lengkap
            </label>
            <input type="text" placeholder='Nama Lengkap' value={nama} onChange={(e) => setNama(e.target.value)} className='w-full border p-2 rounded-md' />
            </div>

            <div>
            <label className="pl-1 block text-black dark:text-white">
                  NIM (Nomor Induk MAHASISWA)
            </label>
            <input type="nim" placeholder='NIM (Nomor Induk Mahasiswa)' value={nim} onChange={(e) => setNim(e.target.value)} className='w-full border p-2 rounded-md' />
            </div>

            <div>
            <label className="pl-1 block text-black dark:text-white">
                  Email
            </label>
            <input type="email" placeholder='Email' value={email} onChange={(e) => setEmail(e.target.value)} className='w-full border p-2 rounded-md' />
            </div>

            <div>
            <label className="pl-1 block text-black dark:text-white">
                  Jenis Kelamin
            </label>
            <div className="mt-2">
                <select value={jenis_kelamin} onChange={(e) => setJenisKelamin(e.target.value)}
                  id="jenis_kelamin"
                  name="jenis_kelamin"
                  className='w-full border p-2 rounded-md'
                >
                  <option>Pilih Jenis Kelamin</option>
                  <option>Laki-laki</option>
                  <option>Perempuan</option>
                </select>
              </div>
              </div>

              <div>
              <label className="pl-1 block text-black dark:text-white">
                  Alamat
            </label>
            <div className="mt-2">
                <textarea
                  id="alamat"
                  name="alamat"
                  rows={3}
                  className='w-full border p-2 rounded-md'
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                />
              </div>
              </div>
            <div>

            <label className="pl-1 block text-black dark:text-white">
                  Nomor Telepon
            </label>
            <input type="text" placeholder='Nomor Telepon' value={telp} onChange={(e) => setTelp(e.target.value)} className='w-full border p-2 rounded-md' />
            </div>
            
            <div>
            <label className="pl-1 block text-black dark:text-white">
                 Foto
            </label>

            <input
                  type="file"
                  name="image"
                  className='w-full border p-2 rounded-md'
                />
            </div>

           <button disabled={isLoading}
                  type="submit"
                  className="rounded-md bg-black px-3 py-2 text-white text-sm font-semibold shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-gray-600"
                  >{isLoading ? "Loading..." : "Submit"}
                </button>
        </form>
    )
}

export default Page