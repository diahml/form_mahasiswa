import { PrismaClient } from "@prisma/client";
import { NextRequest, NextResponse } from "next/server";


const prisma = new PrismaClient()

export const GET = async(req: NextRequest) =>{
    const mahasiswa = await prisma.mahasiswa.findMany({})
    return NextResponse.json({mahasiswa})
}

export const POST = async (req:NextRequest)=>{

    const {email, nama , nim, jenis_kelamin, alamat, telp, image} = await req.json()

    const mahasiswa = await prisma.mahasiswa.create({
        data: {
            email, nama, nim, jenis_kelamin, alamat, telp, image
        }
    })

    return NextResponse.json({mahasiswa})
}

